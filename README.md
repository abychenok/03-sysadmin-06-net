# Домашнее задание к занятию "3.6. Компьютерные сети, лекция 1"

> 1. Работа c HTTP через телнет.
- Подключитесь утилитой телнет к сайту stackoverflow.com
`telnet stackoverflow.com 80`
- отправьте HTTP запрос
```bash
GET /questions HTTP/1.0
HOST: stackoverflow.com
[press enter]
[press enter]
```
- В ответе укажите полученный HTTP код, что он означает?

```
Trying 151.101.193.69...
Connected to stackoverflow.com.
Escape character is '^]'.

GET /questions HTTP/1.0
HOST: stackoverflow.com

HTTP/1.1 301 Moved Permanently
cache-control: no-cache, no-store, must-revalidate
location: https://stackoverflow.com/questions
x-request-guid: 9742eeed-0e33-4d7d-940d-65655b255639
feature-policy: microphone 'none'; speaker 'none'
content-security-policy: upgrade-insecure-requests; frame-ancestors 'self' https://stackexchange.com
Accept-Ranges: bytes
Date: Tue, 10 May 2022 18:33:08 GMT
Via: 1.1 varnish
Connection: close
X-Served-By: cache-bma1660-BMA
X-Cache: MISS
X-Cache-Hits: 0
X-Timer: S1652207589.891550,VS0,VE101
Vary: Fastly-SSL
X-DNS-Prefetch-Control: off
Set-Cookie: prov=bc78d695-0e7d-cc0a-ae62-d5a6dc73aff3; domain=.stackoverflow.com; expires=Fri, 01-Jan-2055 00:00:00 GMT; path=/; HttpOnly

Connection closed by foreign host.
```

Код ответа 301 (перемещено навсегда) -  получаемый в ответ от сервера в ситуации, когда запрошенный ресурс был на постоянной основе перемещён в новое месторасположение, и указывающий на то, что текущие ссылки, использующие данный URL, должны быть обновлены. 
Адрес нового месторасположения ресурса указывается в поле Location получаемого в ответ заголовка пакета протокола HTTP.

> 2. Повторите задание 1 в браузере, используя консоль разработчика F12.
> - откройте вкладку `Network`
> - отправьте запрос http://stackoverflow.com
> - найдите первый ответ HTTP сервера, откройте вкладку `Headers`
> - укажите в ответе полученный HTTP код.

Код 200
> - проверьте время загрузки страницы, какой запрос обрабатывался дольше всего?

Дольше всего 126 мс, обрабатывался html файл.
> - приложите скриншот консоли браузера в ответ.

![Скриншот](network.png?raw=true "Скриншот")

> 3. Какой IP адрес у вас в интернете?

```
curl 2ip.ru
85.249.41.60
```

> 4. Какому провайдеру принадлежит ваш IP адрес? Какой автономной системе AS? Воспользуйтесь утилитой `whois`

```
whois 85.249.41.60
% IANA WHOIS server
% for more information on IANA, visit http://www.iana.org
% This query returned 1 object

refer:        whois.ripe.net

inetnum:      85.0.0.0 - 85.255.255.255
organisation: RIPE NCC
status:       ALLOCATED

whois:        whois.ripe.net

changed:      2004-04
source:       IANA

# whois.ripe.net

inetnum:        85.249.0.0 - 85.249.255.255
netname:        RU-SOVINTEL-20050124
org:            ORG-ES15-RIPE
country:        RU
admin-c:        SVNT2-RIPE
tech-c:         SVNT1-RIPE
status:         ALLOCATED PA
mnt-by:         RIPE-NCC-HM-MNT
mnt-by:         SOVINTEL-MNT
mnt-lower:      ELTEL-RIPE-MNT
mnt-lower:      SOVINTEL-MNT
mnt-routes:     ELTEL-RIPE-MNT
mnt-routes:     SOVINTEL-MNT
mnt-domains:    SOVINTEL-MNT
created:        2005-01-24T11:44:05Z
last-modified:  2016-10-27T13:06:35Z
source:         RIPE

organisation:   ORG-ES15-RIPE
org-name:       PJSC "Vimpelcom"
country:        RU
org-type:       LIR
remarks:        VEON Group
address:        8 Marta str, 10, bld. 14
address:        127083
address:        Moscow
address:        RUSSIAN FEDERATION
phone:          +74999233775
fax-no:         +74957871990
admin-c:        SVNT1-RIPE
admin-c:        SVNT2-RIPE
admin-c:        IAI1-RIPE
admin-c:        DM3740-RIPE
admin-c:        BEE15-RIPE
mnt-ref:        SOVINTEL-MNT
mnt-ref:        ROSNIIROS-MNT
mnt-ref:        RIPE-NCC-HM-MNT
mnt-by:         RIPE-NCC-HM-MNT
mnt-by:         SOVINTEL-MNT
abuse-c:        SVNT2-RIPE
created:        2004-04-17T11:58:43Z
last-modified:  2022-04-21T15:45:58Z
source:         RIPE # Filtered

role:           Sovintel NOC
remarks:        now PAO Vimpelcom - formely Sovam Teleport/Teleross
remarks:        aka Sovintel - Golden Telecom
address:        111250 Russia Moscow Krasnokazarmennaya, 12
mnt-by:         SOVINTEL-MNT
org:            ORG-ES15-RIPE
phone:          +7 800 7008061
fax-no:         +7 495 7871010
abuse-mailbox:  abuse-b2b@beeline.ru
admin-c:        IAI1-RIPE
admin-c:        DM3740-RIPE
tech-c:         DM3740-RIPE
tech-c:         SVNT2-RIPE
nic-hdl:        SVNT1-RIPE
created:        2004-05-13T11:50:32Z
last-modified:  2022-04-20T08:31:22Z
source:         RIPE # Filtered

role:           Sovintel Abuse Department
remarks:        now Vimpelcom Business Abuse Department
address:        111250 Russia Moscow, Krasnokazarmennaya, 12
org:            ORG-ES15-RIPE
fax-no:         +7 495 7254300
phone:          +7 495 7871000
nic-hdl:        SVNT2-RIPE
admin-c:        SVNT1-RIPE
tech-c:         SVNT1-RIPE
mnt-by:         SOVINTEL-MNT
created:        2004-05-14T10:21:01Z
last-modified:  2018-11-08T08:46:48Z
source:         RIPE # Filtered
abuse-mailbox:  abuse-b2b@beeline.ru

% Information related to '85.249.40.0/21AS3216'

route:          85.249.40.0/21
descr:          SOVINTEL/EDN ex eLTeL
origin:         AS3216
mnt-by:         AS3216-MNT
created:        2019-07-29T08:14:37Z
last-modified:  2019-07-29T08:14:37Z
source:         RIPE # Filtered

% This query was served by the RIPE Database Query Service version 1.103 (HEREFORD)
```
Провайдер - PJSC "Vimpelcom"
Автономная сеть - AS3216

> 5. Через какие сети проходит пакет, отправленный с вашего компьютера на адрес 8.8.8.8? Через какие AS? Воспользуйтесь утилитой `traceroute`

```
sudo traceroute -An 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
1  172.16.77.1 [*]  2.712 ms  2.670 ms  2.666 ms
2  * * *
3  * * *
4  * * *
5  72.14.198.182 [AS15169]  5.178 ms 195.14.32.22 [AS8402]  4.682 ms  5.102 ms
6  * * *
7  72.14.233.90 [AS15169]  4.216 ms 108.170.227.90 [AS15169]  3.221 ms 172.253.69.146 [AS15169]  4.591 ms
8  108.170.250.99 [AS15169]  4.570 ms 108.170.250.130 [AS15169]  4.110 ms 108.170.250.113 [AS15169]  5.039 ms
9  172.253.66.116 [AS15169]  20.372 ms 142.251.237.154 [AS15169]  21.400 ms 209.85.249.158 [AS15169]  24.372 ms
10  66.249.95.224 [AS15169]  18.017 ms  22.316 ms 74.125.253.94 [AS15169]  17.187 ms
11  172.253.79.169 [AS15169]  18.965 ms 209.85.251.41 [AS15169]  21.948 ms 216.239.46.243 [AS15169]  22.822 ms
12  * * *
13  * * *
14  * * *
15  * * *
16  * * *
17  * * *
18  * * *
19  * * *
20  * * *
21  8.8.8.8 [AS15169]  19.836 ms *  20.923 ms
```

> 6. Повторите задание 5 в утилите `mtr`. На каком участке наибольшая задержка - delay?

![Скриншот](mtr.png?raw=true "Скриншот")
Наибольшая задержка на этапе передачи пакета на узел AS15169  216.239.48.224

> 7. Какие DNS сервера отвечают за доменное имя dns.google? Какие A записи? воспользуйтесь утилитой `dig`

NS сервера:
```
dig NS dns.google

; <<>> DiG 9.10.6 <<>> NS dns.google
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 59521
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 4, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;dns.google.			IN	NS

;; ANSWER SECTION:
dns.google.		21600	IN	NS	ns1.zdns.google.
dns.google.		21600	IN	NS	ns2.zdns.google.
dns.google.		21600	IN	NS	ns3.zdns.google.
dns.google.		21600	IN	NS	ns4.zdns.google.

;; Query time: 50 msec
;; SERVER: 172.16.76.12#53(172.16.76.12)
;; WHEN: Tue May 10 22:56:23 MSK 2022
;; MSG SIZE  rcvd: 116
```

A записи:
```
dig dns.google

; <<>> DiG 9.10.6 <<>> dns.google
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 11335
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;dns.google.			IN	A

;; ANSWER SECTION:
dns.google.		740	IN	A	8.8.4.4
dns.google.		740	IN	A	8.8.8.8

;; Query time: 70 msec
;; SERVER: 172.16.76.12#53(172.16.76.12)
;; WHEN: Tue May 10 22:54:57 MSK 2022
;; MSG SIZE  rcvd: 71
```


> 8. Проверьте PTR записи для IP адресов из задания 7. Какое доменное имя привязано к IP? воспользуйтесь утилитой `dig`
 PTR запись 8.8.8.8:
 
 ```
 dig -x 8.8.8.8

; <<>> DiG 9.10.6 <<>> -x 8.8.8.8
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 34249
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;8.8.8.8.in-addr.arpa.		IN	PTR

;; ANSWER SECTION:
8.8.8.8.in-addr.arpa.	82114	IN	PTR	dns.google.

;; Query time: 45 msec
;; SERVER: 172.16.76.12#53(172.16.76.12)
;; WHEN: Tue May 10 22:58:14 MSK 2022
;; MSG SIZE  rcvd: 73
```
PTR запись 8.8.4.4:
```
dig -x 8.8.4.4

; <<>> DiG 9.10.6 <<>> -x 8.8.4.4
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 57443
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;4.4.8.8.in-addr.arpa.		IN	PTR

;; ANSWER SECTION:
4.4.8.8.in-addr.arpa.	79421	IN	PTR	dns.google.

;; Query time: 24 msec
;; SERVER: 172.16.76.12#53(172.16.76.12)
;; WHEN: Tue May 10 22:58:36 MSK 2022
;; MSG SIZE  rcvd: 73
```
Имя привязанное к IP адресам `dns.google.`